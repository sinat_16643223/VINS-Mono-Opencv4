#include "feature_tracker.h"

int FeatureTracker::n_id = 0;

bool inBorder(const cv::Point2f &pt)
{
    const int BORDER_SIZE = 1;
    int img_x = cvRound(pt.x);
    int img_y = cvRound(pt.y);
    return BORDER_SIZE <= img_x && img_x < COL - BORDER_SIZE && BORDER_SIZE <= img_y && img_y < ROW - BORDER_SIZE;
}

void reduceVector(vector<cv::Point2f> &v, vector<uchar> status)
{
    int j = 0;
    for (int i = 0; i < int(v.size()); i++)
        if (status[i])
            v[j++] = v[i];
    v.resize(j);
}

void reduceVector(vector<int> &v, vector<uchar> status)
{
    int j = 0;
    for (int i = 0; i < int(v.size()); i++)
        if (status[i])
            v[j++] = v[i];
    v.resize(j);
}


FeatureTracker::FeatureTracker()
{
}

void FeatureTracker::setMask()
{
    if(FISHEYE)
        mask = fisheye_mask.clone();
    else
        mask = cv::Mat(ROW, COL, CV_8UC1, cv::Scalar(255));
    

    // prefer to keep features that are tracked for long time
    vector<pair<int, pair<cv::Point2f, int>>> cnt_pts_id;

    for (unsigned int i = 0; i < forw_pts.size(); i++)
        cnt_pts_id.push_back(make_pair(track_cnt[i], make_pair(forw_pts[i], ids[i])));

    sort(cnt_pts_id.begin(), cnt_pts_id.end(), [](const pair<int, pair<cv::Point2f, int>> &a, const pair<int, pair<cv::Point2f, int>> &b)
         {
            return a.first > b.first;
         });

    forw_pts.clear();
    ids.clear();
    track_cnt.clear();

    for (auto &it : cnt_pts_id)
    {
        if (mask.at<uchar>(it.second.first) == 255)
        {
            forw_pts.push_back(it.second.first);
            ids.push_back(it.second.second);
            track_cnt.push_back(it.first);
            cv::circle(mask, it.second.first, MIN_DIST, 0, -1);
        }
    }
}

void FeatureTracker::addPoints()
{
    for (auto &p : n_pts)
    {
        forw_pts.push_back(p);
        ids.push_back(-1);
        track_cnt.push_back(1);
    }
}

struct sort_points_by_response {
    inline bool operator()(const cv::KeyPoint& a, const cv::KeyPoint& b) {
        return ( a.response > b.response );
    }
};

std::vector<cv::KeyPoint> eigenMatrixToKeyPoints(const Eigen::Matrix<double, 259, Eigen::Dynamic>& eigenMatrix) {
    std::vector<cv::KeyPoint> keypoints;
    
    for (int col = 0; col < eigenMatrix.cols(); ++col) {
        double x = eigenMatrix(0, col);  // 获取 x 坐标
        double y = eigenMatrix(1, col);  // 获取 y 坐标
        
        // 创建 cv::KeyPoint 对象，并设置属性值
        cv::KeyPoint keypoint(x, y, 1.0);  // 设置尺度为 1.0，得分和方向使用默认值
        
        keypoints.push_back(keypoint);  // 添加到关键点列表
    }
    
    return keypoints;
}

void FeatureTracker::readImage(const cv::Mat &_img, double _cur_time)
{
    cv::Mat img;
    TicToc t_r;
    cur_time = _cur_time;
    
        std::string config_path = "/home/nvidia/vinsmonoopencv4_ws/src/VINS-Mono-Opencv4/feature_tracker/SuperPoint-SuperGlue-TensorRT/config/euroc_config.yaml";
        std::string model_dir = "/home/nvidia/vinsmonoopencv4_ws/src/VINS-Mono-Opencv4/feature_tracker/SuperPoint-SuperGlue-TensorRT/weights";
        Configs configs(config_path, model_dir);
        int width = configs.superglue_config.image_width;
        int height = configs.superglue_config.image_height;
        //cv::resize(cur_img, cur_img, cv::Size(width, height));  //这步看看对SLAM有没有影响可不可以不弄？
        //cv::resize(forw_img, forw_img, cv::Size(width, height));  //这步看看对SLAM有没有影响可不可以不弄？

        auto superpoint = std::make_shared<SuperPoint>(configs.superpoint_config);
        //应该是superpoint->build()这步第一次需要消耗比较长时间
        if (!superpoint->build()){
            std::cerr << "Error in SuperGlue building engine. Please check your onnx model path." << std::endl;
            //return 0;
        }
        std::cout << "SuperPoint and SuperGlue inference engine build success." << std::endl;

    if (EQUALIZE)
    {
        cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE(3.0, cv::Size(8, 8));
        TicToc t_c;
        clahe->apply(_img, img);
        ROS_DEBUG("CLAHE costs: %fms", t_c.toc());
    }
    else
        img = _img;

    if (forw_img.empty())
    {
        prev_img = cur_img = forw_img = img;
    }
    else
    {
        forw_img = img;
    }

    forw_pts.clear();
    
    cv::resize(cur_img, cur_img, cv::Size(width, height));  //参考superpoint的示例代码inference_image.cpp加的
    cv::resize(forw_img, forw_img, cv::Size(width, height));  //参考superpoint的示例代码inference_image.cpp加的

    if (cur_pts.size() > 0)
    {
        TicToc t_o;
        vector<uchar> status;
        vector<float> err;
        
        /***
        std::string config_path = "/home/nvidia/vinsmonoopencv4_ws/src/VINS-Mono-Opencv4/feature_tracker/SuperPoint-SuperGlue-TensorRT/config/config.yaml";
        std::string model_dir = "/home/nvidia/vinsmonoopencv4_ws/src/VINS-Mono-Opencv4/feature_tracker/SuperPoint-SuperGlue-TensorRT/weights";
        Configs configs(config_path, model_dir);
        int width = configs.superglue_config.image_width;
        int height = configs.superglue_config.image_height;
        //cv::resize(cur_img, cur_img, cv::Size(width, height));  //这步看看对SLAM有没有影响可不可以不弄？
        //cv::resize(forw_img, forw_img, cv::Size(width, height));  //这步看看对SLAM有没有影响可不可以不弄？

        auto superpoint = std::make_shared<SuperPoint>(configs.superpoint_config);
        //应该是superpoint->build()这步第一次需要消耗比较长时间
        if (!superpoint->build()){
            std::cerr << "Error in SuperGlue building engine. Please check your onnx model path." << std::endl;
            //return 0;
        }
        std::cout << "SuperPoint and SuperGlue inference engine build success." << std::endl;
        ***/

        //Eigen::Matrix<double, 259, Eigen::Dynamic> feature_points0, feature_points1;
        //superpoint->infer(cur_img, feature_points0);
        
        cv::calcOpticalFlowPyrLK(cur_img, forw_img, cur_pts, forw_pts, status, err, cv::Size(21, 21), 3);

        for (int i = 0; i < int(forw_pts.size()); i++)
            if (status[i] && !inBorder(forw_pts[i]))
                status[i] = 0;
        reduceVector(prev_pts, status);
        reduceVector(cur_pts, status);
        reduceVector(forw_pts, status);
        reduceVector(ids, status);
        reduceVector(cur_un_pts, status);
        reduceVector(track_cnt, status);
        ROS_DEBUG("temporal optical flow costs: %fms", t_o.toc());
    }

    for (auto &n : track_cnt)
        n++;

    if (PUB_THIS_FRAME)
    {
        rejectWithF();
        ROS_DEBUG("set mask begins");
        TicToc t_m;
        setMask();
        ROS_DEBUG("set mask costs %fms", t_m.toc());

        ROS_DEBUG("detect feature begins");
        TicToc t_t;
        int n_max_cnt = MAX_CNT - static_cast<int>(forw_pts.size());
        if (n_max_cnt > 0)
        {
            if(mask.empty())
                cout << "mask is empty " << endl;
            if (mask.type() != CV_8UC1)
                cout << "mask type wrong " << endl;
            if (mask.size() != forw_img.size())
                cout << "wrong size " << endl;
            
            //superpoint
            if(1)
            {
            //基于FAST的改的
            n_pts.clear();
            
            Eigen::Matrix<double, 259, Eigen::Dynamic> feature_points0, feature_points1;
            superpoint->infer(forw_img, feature_points0);
            
            std::vector<cv::KeyPoint> keyPnt;
            for(size_t i = 0; i < feature_points0.cols(); ++i){
               double score = feature_points0(0, i);
               double x = feature_points0(1, i);
               double y = feature_points0(2, i);
               keyPnt.emplace_back(x, y, 8, -1, score);
              }
            //keyPnt = eigenMatrixToKeyPoints(feature_points0); //把Eigen::Matrix<double, 259, Eigen::Dynamic>类型转为std::vectorcv::KeyPoint类型
            //FASTextractor.ComputeFASTkeyPoints(forw_img, keyPnt);
            if(!mask.empty()) {
                for(size_t keyCounter = 0; keyCounter < keyPnt.size(); keyCounter++) {
                    cv::Point2f kp = keyPnt[keyCounter].pt;
                    if( mask.at<uchar>(kp) == 0) {  
                        keyPnt.erase( keyPnt.begin() + keyCounter );
                        keyCounter--;
                    }
                }
            }
            if(keyPnt.size() > n_max_cnt) {
                std::sort(keyPnt.begin(), keyPnt.end(), sort_points_by_response());
                for(int i = 0; i < n_max_cnt;i++) 
                    n_pts.push_back(keyPnt[i].pt);
            }
            else {
                for(int i = 0; i < keyPnt.size(); i++) 
                    n_pts.push_back(keyPnt[i].pt);
            }

            }  
                
            //FAST
            if(0)
            {
            // use FAST 
            n_pts.clear();
            std::vector<cv::KeyPoint> keyPnt;
            FASTextractor.ComputeFASTkeyPoints(forw_img, keyPnt);
            if(!mask.empty()) {
                for(size_t keyCounter = 0; keyCounter < keyPnt.size(); keyCounter++) {
                    cv::Point2f kp = keyPnt[keyCounter].pt;
                    if( mask.at<uchar>(kp) == 0) {  
                        keyPnt.erase( keyPnt.begin() + keyCounter );
                        keyCounter--;
                    }
                }
            }
            if(keyPnt.size() > n_max_cnt) {
                std::sort(keyPnt.begin(), keyPnt.end(), sort_points_by_response());
                for(int i = 0; i < n_max_cnt;i++) 
                    n_pts.push_back(keyPnt[i].pt);
            }
            else {
                for(int i = 0; i < keyPnt.size(); i++) 
                    n_pts.push_back(keyPnt[i].pt);
            }

            }
            
            //Harris
            if(0)
            {
            cv::goodFeaturesToTrack(forw_img, n_pts, MAX_CNT - forw_pts.size(), 0.01, MIN_DIST, mask);
            }
        }
        else
            n_pts.clear();
        ROS_DEBUG("detect feature costs: %fms", t_t.toc());

        ROS_DEBUG("add feature begins");
        TicToc t_a;
        addPoints();
        ROS_DEBUG("selectFeature costs: %fms", t_a.toc());
    }
    prev_img = cur_img;
    prev_pts = cur_pts;
    prev_un_pts = cur_un_pts;
    cur_img = forw_img;
    cur_pts = forw_pts;
    undistortedPoints();
    prev_time = cur_time;
}

void FeatureTracker::rejectWithF()
{
    if (forw_pts.size() >= 8)
    {
        ROS_DEBUG("FM ransac begins");
        TicToc t_f;
        vector<cv::Point2f> un_cur_pts(cur_pts.size()), un_forw_pts(forw_pts.size());
        for (unsigned int i = 0; i < cur_pts.size(); i++)
        {
            Eigen::Vector3d tmp_p;
            m_camera->liftProjective(Eigen::Vector2d(cur_pts[i].x, cur_pts[i].y), tmp_p);
            tmp_p.x() = FOCAL_LENGTH * tmp_p.x() / tmp_p.z() + COL / 2.0;
            tmp_p.y() = FOCAL_LENGTH * tmp_p.y() / tmp_p.z() + ROW / 2.0;
            un_cur_pts[i] = cv::Point2f(tmp_p.x(), tmp_p.y());

            m_camera->liftProjective(Eigen::Vector2d(forw_pts[i].x, forw_pts[i].y), tmp_p);
            tmp_p.x() = FOCAL_LENGTH * tmp_p.x() / tmp_p.z() + COL / 2.0;
            tmp_p.y() = FOCAL_LENGTH * tmp_p.y() / tmp_p.z() + ROW / 2.0;
            un_forw_pts[i] = cv::Point2f(tmp_p.x(), tmp_p.y());
        }

        vector<uchar> status;
        cv::findFundamentalMat(un_cur_pts, un_forw_pts, cv::FM_RANSAC, F_THRESHOLD, 0.99, status);
        int size_a = cur_pts.size();
        reduceVector(prev_pts, status);
        reduceVector(cur_pts, status);
        reduceVector(forw_pts, status);
        reduceVector(cur_un_pts, status);
        reduceVector(ids, status);
        reduceVector(track_cnt, status);
        ROS_DEBUG("FM ransac: %d -> %lu: %f", size_a, forw_pts.size(), 1.0 * forw_pts.size() / size_a);
        ROS_DEBUG("FM ransac costs: %fms", t_f.toc());
    }
}

bool FeatureTracker::updateID(unsigned int i)
{
    if (i < ids.size())
    {
        if (ids[i] == -1)
            ids[i] = n_id++;
        return true;
    }
    else
        return false;
}

void FeatureTracker::readIntrinsicParameter(const string &calib_file)
{
    ROS_INFO("reading paramerter of camera %s", calib_file.c_str());
    m_camera = CameraFactory::instance()->generateCameraFromYamlFile(calib_file);
}

void FeatureTracker::showUndistortion(const string &name)
{
    cv::Mat undistortedImg(ROW + 600, COL + 600, CV_8UC1, cv::Scalar(0));
    vector<Eigen::Vector2d> distortedp, undistortedp;
    for (int i = 0; i < COL; i++)
        for (int j = 0; j < ROW; j++)
        {
            Eigen::Vector2d a(i, j);
            Eigen::Vector3d b;
            m_camera->liftProjective(a, b);
            distortedp.push_back(a);
            undistortedp.push_back(Eigen::Vector2d(b.x() / b.z(), b.y() / b.z()));
            //printf("%f,%f->%f,%f,%f\n)\n", a.x(), a.y(), b.x(), b.y(), b.z());
        }
    for (int i = 0; i < int(undistortedp.size()); i++)
    {
        cv::Mat pp(3, 1, CV_32FC1);
        pp.at<float>(0, 0) = undistortedp[i].x() * FOCAL_LENGTH + COL / 2;
        pp.at<float>(1, 0) = undistortedp[i].y() * FOCAL_LENGTH + ROW / 2;
        pp.at<float>(2, 0) = 1.0;
        //cout << trackerData[0].K << endl;
        //printf("%lf %lf\n", p.at<float>(1, 0), p.at<float>(0, 0));
        //printf("%lf %lf\n", pp.at<float>(1, 0), pp.at<float>(0, 0));
        if (pp.at<float>(1, 0) + 300 >= 0 && pp.at<float>(1, 0) + 300 < ROW + 600 && pp.at<float>(0, 0) + 300 >= 0 && pp.at<float>(0, 0) + 300 < COL + 600)
        {
            undistortedImg.at<uchar>(pp.at<float>(1, 0) + 300, pp.at<float>(0, 0) + 300) = cur_img.at<uchar>(distortedp[i].y(), distortedp[i].x());
        }
        else
        {
            //ROS_ERROR("(%f %f) -> (%f %f)", distortedp[i].y, distortedp[i].x, pp.at<float>(1, 0), pp.at<float>(0, 0));
        }
    }
    cv::imshow(name, undistortedImg);
    cv::waitKey(0);
}

void FeatureTracker::undistortedPoints()
{
    cur_un_pts.clear();
    cur_un_pts_map.clear();
    //cv::undistortPoints(cur_pts, un_pts, K, cv::Mat());
    for (unsigned int i = 0; i < cur_pts.size(); i++)
    {
        Eigen::Vector2d a(cur_pts[i].x, cur_pts[i].y);
        Eigen::Vector3d b;
        m_camera->liftProjective(a, b);
        cur_un_pts.push_back(cv::Point2f(b.x() / b.z(), b.y() / b.z()));
        cur_un_pts_map.insert(make_pair(ids[i], cv::Point2f(b.x() / b.z(), b.y() / b.z())));
        //printf("cur pts id %d %f %f", ids[i], cur_un_pts[i].x, cur_un_pts[i].y);
    }
    // caculate points velocity
    if (!prev_un_pts_map.empty())
    {
        double dt = cur_time - prev_time;
        pts_velocity.clear();
        for (unsigned int i = 0; i < cur_un_pts.size(); i++)
        {
            if (ids[i] != -1)
            {
                std::map<int, cv::Point2f>::iterator it;
                it = prev_un_pts_map.find(ids[i]);
                if (it != prev_un_pts_map.end())
                {
                    double v_x = (cur_un_pts[i].x - it->second.x) / dt;
                    double v_y = (cur_un_pts[i].y - it->second.y) / dt;
                    pts_velocity.push_back(cv::Point2f(v_x, v_y));
                }
                else
                    pts_velocity.push_back(cv::Point2f(0, 0));
            }
            else
            {
                pts_velocity.push_back(cv::Point2f(0, 0));
            }
        }
    }
    else
    {
        for (unsigned int i = 0; i < cur_pts.size(); i++)
        {
            pts_velocity.push_back(cv::Point2f(0, 0));
        }
    }
    prev_un_pts_map = cur_un_pts_map;
}
